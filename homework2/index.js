/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */

class Hamburger {

    constructor(size, stuffing) {
        try {
            this.size = size;
            if (!this.size) {
                throw new HamburgerException('no size given')
            } else if (this.size.type !== 'size') {
                throw new HamburgerException('invalid size', this.size.name)
            }
            this.stuffing = stuffing;
            if (!this.stuffing) {
                throw new HamburgerException('no stuffing given')
            } else if (this.size.type !== 'size') {
                throw new HamburgerException('invalid stuffing', this.stuffing.name)
            }
            this.hamburger = [size, stuffing]; // [ {size}, {stuffing} ]
        }  catch (error) {
            console.log(`${error.name}: ${error.message} ==> ${error.cause}`)
        }

    }

    static get _sizeSmall() {
        return {price: 50, calories: 20, type: 'size', name: 'small'};
    }
    static get _sizeLarge() {
        return {price: 100, calories: 50, type: 'size', name: 'large'};
    }

    static get _stuffingCheese() {
        return {price: 10, calories: 20, type: 'stuffing', name: 'cheese'};
    }
    static get _stuffingSalad() {
        return {price: 20, calories: 5, type: 'stuffing', name: 'salad'};
    }
    static get _stuffingPotato() {
        return {price: 15, calories: 10, type: 'stuffing', name: 'potato'};
    }

    static get _toppingMayo() {
        return {price: 15, calories: 0, type: 'topping', name: 'mayo'};
    }
    static get _toppingSpice() {
        return {price: 20, calories: 5, type: 'topping', name: 'spice'};
    }


    /**
     * Добавить добавку к гамбургеру. Можно добавить несколько
     * добавок, при условии, что они разные.
     *
     * @param topping     Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    addTopping(topping) {
        try {
            if (!topping || topping.type !== 'topping') {
                throw new HamburgerException('invalid topping')
            }

            let duplicate = this.hamburger.find(obj => obj.name === topping.name); // {} || undefined
            if (duplicate) {
                throw new HamburgerException('duplicate topping', topping.name)
            }


            this.hamburger.push(topping);
        } catch (error) {
            console.log(`${error.name}: ${error.message} ==> ${error.cause}`)
        }

    };

    /**
     * Убрать добавку, при условии, что она ранее была
     * добавлена.
     *
     * @param topping   Тип добавки
     * @throws {HamburgerException}  При неправильном использовании
     */
    removeTopping(topping) {
        try {
            if (topping.type !== 'topping') {
                throw new HamburgerException('Cannot be removed ==> invalid topping')
            }
            let index = this.hamburger.indexOf(topping);
            this.hamburger.splice(index, 1);
        } catch (error) {
            console.log(`${error.name}: ${error.message}`)
        }
    };


    /**
     * Получить список добавок.
     *
     * @return {Array} Массив добавленных добавок, содержит константы
     *                 Hamburger.TOPPING_*
     */
    getToppings() {
        return this.hamburger.filter(obj => {
            return obj.type === 'topping'
        })

    };

    /**
     * Узнать размер гамбургера
     */
    getSize() {
        return JSON.stringify(this.size) === JSON.stringify(Hamburger._sizeSmall) ? Hamburger._sizeSmall : Hamburger._sizeLarge

    };

    /**
     * Узнать начинку гамбургера
     */
    getStuffing() {
        return this.stuffing.name;
    };

    /**
     * Узнать цену гамбургера
     * @return {Number} Цена в тугриках
     */
    calculatePrice() {
        return this.hamburger.reduce((total, current) => {
            return total + current.price;
        }, 0)
    };

        /**
     * Узнать калорийность
     * @return {Number} Калорийность в калориях
     */
    calculateCalories() {
        return this.hamburger.reduce((total, current) => {
            return total + current.calories;
        }, 0)
    };

}



class HamburgerException {

    constructor(message, cause) {
        this.message = message;
        this.cause = cause;
        this.name = 'HamburgerException';
    }
}

//маленький гамбургер с начинкой из сыра
const hamburger1 = new Hamburger(Hamburger._sizeSmall, Hamburger._stuffingCheese);
//добавка из майонеза
hamburger1.addTopping(Hamburger._toppingMayo);
// спросим сколько там калорий
console.log("Calories: %f", hamburger1.calculateCalories()); // 40
// сколько стоит
console.log("Price: %f", hamburger1.calculatePrice()); // 75
// я тут передумал и решил добавить еще приправу
hamburger1.addTopping(Hamburger._toppingSpice);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger1.calculatePrice()); // 95
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger1.getSize() === Hamburger._sizeLarge); // -> false
// Убрать добавку
hamburger1.removeTopping(Hamburger._toppingSpice);
// Сколько у нас добавок?
console.log("Have %d toppings", hamburger1.getToppings().length); // 1
// Узнать начинку
console.log("Stuffing is %s", hamburger1.getStuffing());


// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger._toppingSpice);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger._sizeSmall, Hamburger._stuffingCheese);
// h4.addTopping(Hamburger._toppingMayo);
// h4.addTopping(Hamburger._toppingMayo);
// // HamburgerException: duplicate topping 'TOPPING_MAYO'
// h4.removeTopping(Hamburger._sizeSmall);
// //  => HamburgerException: Cannot be removed ==> invalid topping
