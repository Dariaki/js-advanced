/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
function Hamburger(size, stuffing) {

        try {
            this.size = size;
            if (!this.size) {
                throw new HamburgerException('no size given')
            } else if (this.size.type !== 'size') {
                throw new HamburgerException('invalid size', this.size.name)
            }

            this.stuffing = stuffing;
            if (!this.stuffing) {
                throw new HamburgerException('no stuffing given')
            } else if (this.size.type !== 'size') {
                throw new HamburgerException('invalid stuffing', this.stuffing.name)
            }
            this.hamburger = [size, stuffing]; // [ {size}, {stuffing} ]


        } catch (error) {
            console.log(`${error.name}: ${error.message} ==> ${error.cause}`)
        }
}

Hamburger.SIZE_SMALL = {price: 50, calories: 20, type: 'size', name: 'small'};
Hamburger.SIZE_LARGE = {price: 100, calories: 50, type: 'size', name: 'large'};

Hamburger.STUFFING_CHEESE = {price: 10, calories: 20, type: 'stuffing', name: 'cheese'};
Hamburger.STUFFING_SALAD = {price: 20, calories: 5, type: 'stuffing', name: 'salad'};
Hamburger.STUFFING_POTATO = {price: 15, calories: 10, type: 'stuffing', name: 'potato'};

Hamburger.TOPPING_MAYO = {price: 15, calories: 0, type: 'topping', name: 'mayo'};
Hamburger.TOPPING_SPICE = {price: 20, calories: 5, type: 'topping', name: 'spice'};


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    try {
            if (!topping || topping.type !== 'topping') {
                throw new HamburgerException('invalid topping')
            }
            var duplicate;
            for (var i = 0; i < this.hamburger.length; i++) {
                if (this.hamburger[i].name === topping.name) {
                    duplicate = this.hamburger[i];
                }
            }
            if (duplicate) {
                throw new HamburgerException('duplicate topping', topping.name)
            }

        this.hamburger.push(topping);
        }
    catch (error) {
            console.log(`${error.name}: ${error.message} ==> ${error.cause}`)
    }
};


/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (topping.type !== 'topping') {
            throw new HamburgerException('Cannot be removed ==> invalid topping')
        }

        var index = this.hamburger.indexOf(topping);
        this.hamburger.splice(index, 1);
    } catch (error) {
        console.log(`${error.name}: ${error.message}`)
    }

};

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {
    var toppingsArr = [];
    for (var i = 0; i < this.hamburger.length; i++) {
        if (this.hamburger[i].type === 'topping') {
            toppingsArr.push(this.hamburger[i]);
        }
    }
    return toppingsArr;

};

/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function () {
    if (JSON.stringify(this.size) === JSON.stringify(Hamburger.SIZE_SMALL)) {
        return Hamburger.SIZE_SMALL;
    } else {
        return Hamburger.SIZE_LARGE
    }

};


/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function () {
    return this.stuffing.name;
};

/**
 * Узнать цену гамбургера
 * @return {Number} Цена в тугриках
 */
Hamburger.prototype.calculatePrice = function () {
    var sum = 0;
    for (var i = 0; i < this.hamburger.length; i++) {
        sum += this.hamburger[i].price;
    }
    return sum;

};
/**
 * Узнать калорийность
 * @return {Number} Калорийность в калориях
 */
Hamburger.prototype.calculateCalories = function () {
    var sum = 0;
    for (var i = 0; i < this.hamburger.length; i++) {
        sum += this.hamburger[i].calories;
    }
    return sum;
};


function HamburgerException(message, cause) {
    this.message = message;
    this.cause = cause;
    this.name = 'HamburgerException';
}


//маленький гамбургер с начинкой из сыра
const hamburger1 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
//добавка из майонеза
hamburger1.addTopping(Hamburger.TOPPING_MAYO);
// спросим сколько там калорий
console.log("Calories: %f", hamburger1.calculateCalories()); // 40
// сколько стоит
console.log("Price: %f", hamburger1.calculatePrice()); // 75
// я тут передумал и решил добавить еще приправу
hamburger1.addTopping(Hamburger.TOPPING_SPICE);
// А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger1.calculatePrice()); // 95
// Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger1.getSize() === Hamburger.SIZE_LARGE); // -> false
// Убрать добавку
hamburger1.removeTopping(Hamburger.TOPPING_SPICE);
// Сколько у нас добавок?
console.log("Have %d toppings", hamburger1.getToppings().length); // 1
// Узнать начинку
console.log("Stuffing is %s", hamburger1.getStuffing());


// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SPICE'
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// h4.addTopping(Hamburger.TOPPING_MAYO);
// h4.addTopping(Hamburger.TOPPING_MAYO);
//  => HamburgerException: duplicate topping 'TOPPING_MAYO'
// h4.addTopping(Hamburger.SIZE_SMALL);
// h4.removeTopping(Hamburger.SIZE_SMALL);
// //  => HamburgerException: Cannot be removed ==> invalid topping
